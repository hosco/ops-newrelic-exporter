"""
See README
"""

import os
import requests
from prometheus_client import Gauge, Summary, start_http_server

REQUEST_TIME = Summary('get_metrics_seconds', 'Time to process get metrics', [])

class NewRelicAPI:
    """Get applications and metrics from NewRelic"""
    applications = []

    ### Application summary
    asResponseTime = Gauge(
        'newrelic_application_summary_response_time_milliseconds',
        'Application summary, response time',
        ['application_name', 'application_id']
    )
    asThroughput = Gauge(
        'newrelic_application_summary_throughput_rpm',
        'Application summary, throughput',
        ['application_name', 'application_id']
    )
    asErrorRate = Gauge(
        'newrelic_application_summary_error_rate',
        'Application summary, error rate',
        ['application_name', 'application_id']
    )
    asApdexTarget = Gauge(
        'newrelic_application_summary_apdex_target',
        'Application summary, apdex target',
        ['application_name', 'application_id']
    )
    asApdexScore = Gauge(
        'newrelic_application_summary_apdex_score',
        'Application summary, apdex score',
        ['application_name', 'application_id']
    )
    asHostCount = Gauge(
        'newrelic_application_summary_host_count',
        'Application summary, host count',
        ['application_name', 'application_id']
    )
    asInstanceCount = Gauge(
        'newrelic_application_summary_instance_count',
        'Application summary, instance count',
        ['application_name', 'application_id']
    )

    ### End user summary
    eusResponseTime = Gauge(
        'newrelic_end_user_summary_response_time',
        'End response time, instance count',
        ['application_name', 'application_id']
    )
    eusThroughput = Gauge(
        'newrelic_end_user_summary_throughput',
        'End user summary, throughput',
        ['application_name', 'application_id']
    )
    eusApdexTarget = Gauge(
        'newrelic_end_user_summary_apdex_target',
        'End user summary, apdex target',
        ['application_name', 'application_id']
    )
    eusApdexScore = Gauge(
        'newrelic_end_user_summary_apdex_score',
        'End user summary, apdex score',
        ['application_name', 'application_id']
    )

    ### Settings
    settingsAppApdexThreshold = Gauge(
        'newrelic_settings_app_apdex_threshold',
        'Settings, app apdex threshold',
        ['application_name', 'application_id']
    )
    settingsEndUserApdexThreshold = Gauge(
        'newrelic_settings_end_user_apdex_threshold',
        'Settings, end user apdex threshold',
        ['application_name', 'application_id']
    )

    @staticmethod
    def get_headers():
        """Return header with NewRelic API KEY"""
        return {'X-Api-Key': os.environ['NEWRELIC_API_KEY']}

    def __init__(self):
        self.applications = []

    def set_applications_metrics(self):
        """Set metrics for all applications"""
        self.load_applications()

        for application in self.applications:
            self.set_application_metrics(application)

    def load_applications(self):
        """Return all applications as a dict with id, name, ..."""
        headers = NewRelicAPI.get_headers()
        request = requests.get(
            'https://api.newrelic.com/v2/applications.json',
            headers=headers
        )

        response = request.json()
        if 'applications' in response:
            self.applications = response['applications']

    def set_application_metrics(self, application):
        """Get metrics from an application and update metrics"""
        headers = NewRelicAPI.get_headers()

        try:
            request = requests.get(
                'https://api.newrelic.com/v2/applications/' + str(application['id']) + '.json',
                headers=headers,
                timeout=10
            )

            response = request.json()
        except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout):
            print('Couldn\'t load metrics for ' + str(application['id']))
            return

        if 'application' in response:
            application = response['application']
        else:
            application = {}

        if 'application_summary' in application:
            # pylint: disable=no-member
            self.asResponseTime.labels(application['name'], application['id'])\
                .set(application['application_summary']['response_time'])
            # pylint: disable=no-member
            self.asThroughput.labels(application['name'], application['id'])\
                .set(application['application_summary']['throughput'])
            # pylint: disable=no-member
            self.asErrorRate.labels(application['name'], application['id'])\
                .set(application['application_summary']['error_rate'])

            if 'apdex_target' in application['application_summary']:
                # pylint: disable=no-member
                self.asApdexTarget.labels(application['name'], application['id'])\
                    .set(application['application_summary']['apdex_target'])

            if 'apdex_score' in application['application_summary']:
                # pylint: disable=no-member
                self.asApdexScore.labels(application['name'], application['id'])\
                    .set(application['application_summary']['apdex_score'])

            if 'host_count' in application['application_summary']:
                # pylint: disable=no-member
                self.asHostCount.labels(application['name'], application['id'])\
                    .set(application['application_summary']['host_count'])

            if 'instance_count' in application['application_summary']:
                # pylint: disable=no-member
                self.asInstanceCount.labels(application['name'], application['id'])\
                    .set(application['application_summary']['instance_count'])

        if 'end_user_summary' in application:
            # pylint: disable=no-member
            self.eusResponseTime.labels(application['name'], application['id'])\
                .set(application['end_user_summary']['response_time'])
            # pylint: disable=no-member
            self.eusThroughput.labels(application['name'], application['id'])\
                .set(application['end_user_summary']['throughput'])
            # pylint: disable=no-member
            self.eusApdexTarget.labels(application['name'], application['id'])\
                .set(application['end_user_summary']['apdex_target'])

            if 'apdex_score' in application['end_user_summary']:
                # pylint: disable=no-member
                self.eusApdexScore.labels(application['name'], application['id'])\
                    .set(application['end_user_summary']['apdex_score'])

        if 'settings' in application:
            # pylint: disable=no-member
            self.settingsAppApdexThreshold.labels(application['name'], application['id'])\
                .set(application['settings']['app_apdex_threshold'])
            # pylint: disable=no-member
            self.settingsEndUserApdexThreshold.labels(application['name'], application['id'])\
                .set(application['settings']['end_user_apdex_threshold'])

@REQUEST_TIME.time()
def update_api_one():
    """Get metrics from NewRelic"""
    api = NewRelicAPI()
    api.set_applications_metrics()

start_http_server(9000)

while True:
    print('Update the API')
    update_api_one()
    print('Done - Update the API')
