# OPS NewRelic Exporter

This porject create an HTTP Server to expose every metric from a NewRelic account

## Contribute

* Build the docker image `docker build -t ops-newrelic-exporter .`
* Run it `docker run --rm -it -v ${PWD}:/usr/src/app/ -p 9000:9000 ops-newrelic-exporter bash`
* Launch it with `python src/index.py`

Don't forget to define somewhere your NewRelic API KEY: `NEWRELIC_API_KEY`
