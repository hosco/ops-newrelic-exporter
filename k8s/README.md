# Create a secret

```sh
echo "NEW_RELIC_API_KEY_HERE"  > NEWRELIC_API_KEY
kubectl create secret --namespace=monitoring generic ops-newrelic-exporter --from-file=./NEWRELIC_API_KEY
```
